use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rustc_serialize::base64::{FromBase64, ToBase64, STANDARD};
use tracing::{error, info};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

use jeremytan_ids721_week2::{decrypt, encrypt};
use serde::{Deserialize, Serialize};

// The key to use for cipher
static KEY: [u8; 32] = [
    11, 240, 187, 101, 86, 31, 106, 213, 158, 232, 52, 243, 245, 149, 250, 129, 249, 152, 173, 1,
    82, 169, 242, 238, 28, 88, 67, 186, 127, 88, 129, 245,
];

// The IV to use for cipher
static IV: [u8; 16] = [
    171, 250, 33, 152, 5, 58, 106, 206, 82, 13, 91, 144, 70, 67, 203, 91,
];

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize, Debug)]
struct Request {
    command: String,
    message: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize, Debug)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let command = event.payload.command;
    let message: String = event.payload.message;
    // response variable is changing
    let resp: Response;

    // Encrypt or decrypt based on command
    if command == "encrypt" {
        // encode and prepare response
        let encoded = encrypt((&message).as_bytes(), &KEY, &IV).ok().unwrap();
        let encoded_string: String = encoded.to_base64(STANDARD);
        // Prepare the response
        resp = Response {
            req_id: event.context.request_id,
            msg: format!("Encoded string: {}.", encoded_string),
        };
    } else if command == "decrypt" {
        // decode and prepare response
        let decoded_bytes = match (&message).from_base64() {
            Ok(decoded_bytes) => decoded_bytes,
            Err(err) => {
                error!("Error decoding Base64: {:?}", err);
                return Err(err.into());
            }
        };
        // passes proper format
        let decoded = decrypt(&decoded_bytes, &KEY, &IV).ok().unwrap();
        let decoded_string: String = decoded.iter().map(|&d| d as u8 as char).collect();
        // Prepare the response
        resp = Response {
            req_id: event.context.request_id,
            msg: format!("Decoded string: {}.", decoded_string),
        };
    } else {
        // wrong command
        // Prepare the response
        resp = Response {
            req_id: event.context.request_id,
            msg: format!("Command {} is not valid. Use encrypt or decrypt.", command),
        };
    }

    // Log info about the response
    info!("Response: {:?}", resp);

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encrypt_decrypt() {
        let original_data = b"Hello, World!";

        let encrypted_data = encrypt(original_data, &KEY, &IV).expect("Encryption failed");
        let decrypted_data = decrypt(&encrypted_data, &KEY, &IV).expect("Decryption failed");

        // Convert the data to strings for better error messages in case of failure
        let original_str = String::from_utf8_lossy(original_data);
        let decrypted_str = String::from_utf8_lossy(&decrypted_data);

        assert_eq!(original_data, &decrypted_data[..]);
        println!("Original: {}", original_str);
        println!("Decrypted: {}", decrypted_str);
    }
}
