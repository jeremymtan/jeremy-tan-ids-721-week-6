[![pipeline status](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-week-6/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-week-6/-/commits/main)
# AES-256 CBC PKCS#7 AWS Lambda function with Centeralized Logging
## Purpose of Project
The purpose of this project is to add logging to a lambda function, explore the logging services such as AWS X-ray, and create a centeralized logging. I enhance a previous miniproject to further develop it even more. 

The user would just need to invoke my Lambda function by calling `make aws-invoke` (with the appropriate credentials) or send a request through the API Gateway at the link below. 

## AWS X-ray Tracing 
I simulated some errors to show how it would work with tracing.
Trace Map
![Screenshot_2024-03-06_at_9.15.29_PM](/uploads/e8dd4d4867ccc1dc8a13201a0878ce1c/Screenshot_2024-03-06_at_9.15.29_PM.png)

Traces
![Screenshot_2024-03-25_at_9.24.13_PM](/uploads/3b7d6fe103743ef75d580423d731a541/Screenshot_2024-03-25_at_9.24.13_PM.png)

## Cloudwatch Centeralized Logging 

![Screenshot_2024-03-06_at_9.17.05_PM](/uploads/c80ee3b4faeb883c48575f0e85b8ea20/Screenshot_2024-03-06_at_9.17.05_PM.png)


## Preparation
1. Rust and Cargo Lambda are required for this project: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, and `brew install cargo-lambda`
2. An AWS account is required. Sign up for the free tier.
3. Set up your Cargo Lambda project `cargo lambda new new-lambda-project`
4. Modify the template with your own additions or deletions.
5. Do `cargo lambda watch` to test locally.
6. Do `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test the Lambda function (arguments passed in may be different).
7. Go to the AWS IAM web management console and add an IAM User for credentials.
8. Attach policies `lambdafullaccess` and `iamfullaccess`.
9. Finish user creation, open user details, and go to security credentials.
10. Generate access key.
11. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in a `.env` file that won't be pushed with your repo (add to .gitignore).
12. Export the vars by using `export` in the terminal. This lets cargo lambda know which AWS account to deploy to.
13. Build your project for release `cargo lambda build --release`.
14. Deploy your project for release `cargo lambda deploy`.
15. Login to AWS Lambda (make sure you have the region correct on the top right bar) and check you have it installed.
16. After confirmation, you now want to connect your Lambda function with AWS API Gateway so head over there.
17. Create a new API (keep default settings, REST API) then create a new resource (the URL path that will be appended to your API link).
18. Select the resource, add method type `ANY` and select Lambda function.
19. Turn off Lambda proxy integration if it's on.
20. Deploy your API by adding a new stage (another string that will be appended to your URL path).
21. After clicking deploy, find `stages` and find your invoke URL.


After everything is done, you will have a link like so:
https://a5xyttmdz1.execute-api.us-east-1.amazonaws.com/test/encrypt-decrypt 

You can test your api gateway by sending a curl post request:
```
curl -X POST https://a5xyttmdz1.execute-api.us-east-1.amazonaws.com/test/encrypt-decrypt \
  -H 'content-type: application/json' \
  -d '{ "command": "decrypt", "message": "yVpcf4OnRrbQiGCaFh155A==" }'
```

You can test your lambda fucntion without using the api gateway like so:
`cargo lambda invoke --remote <name of lambda function>`

22. After confirming your api gateway and lambda functions work, add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) to gitlab secrets 
23. Use my `.gitlab-ci.yml` to enable auto build, test, and deploy of your lambda function every time you push your repo
24. Enable Cloudwatch on your lambda function if it isn't already enabled (should be on by default)
25. Add xray tracing by going to Configuration -> Monitoring and operation tools -> Toggle active tracking
26. Find Cloudwatch -> Log Groups -> Find lambda function log group to view centeralized logging
27. You can also add API Gateway Cloud Watch by adding a role that has Cloud Watch permissions to the API Gateway (IAM -> add role -> add service -> search for Cloud Watch -> name your role and create -> copy arn -> go to generl API Gateway settings and your role -> go to your specific API gateway you want to add logging to -> go to stages -> enable Cloud Watch (click options that you are interestedin))


## Diagram of Final Lambda Diagram Integration

![Screenshot_2024-03-06_at_9.31.20_PM](/uploads/361b76e6c35000eccbb56a5cb24a4fe3/Screenshot_2024-03-06_at_9.31.20_PM.png)

## Successful Test Example

![Screenshot_2024-03-06_at_9.31.42_PM](/uploads/28279465f8ea96e919daa3b2d9aed677/Screenshot_2024-03-06_at_9.31.42_PM.png)

## References
1. https://stratusgrid.com/blog/aws-lambda-rust-how-to-deploy-aws-lambda-functions
2. https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
3. https://www.cobbing.dev/blog/grappling-a-rust-lambda/
4. https://www.cargo-lambda.info/commands/deploy.html
5. https://coursera.org/groups/building-rust-aws-lambda-microservices-with-cargo-lambda-rd2pc
6. https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs
7. https://docs.aws.amazon.com/lambda/latest/dg/services-xray.html

